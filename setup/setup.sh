#!/bin/bash

 #####################################
 #                                   #
 #   Setup Script for Privacy Proxy  #
 #                                   #
 #####################################



RED='\033[0;32m'
NC='\033[0m' # No Color


echo -e "${RED}Starting install!${NC}"

echo -e "${RED}Updating system${NC}"

apt-get update
apt-get -y upgrade

apt-get -y install pwgen

echo -e "${RED}Installing mysql!${NC}"


DEBIAN_FRONTEND=noninteractive apt-get -y install mysql-server

mysqlpw=$(pwgen 16 1)
mysqladmin -u root password $mysqlpw

echo -e "${RED}installing other software${NC}"

apt-get -y install apache2 php5 libapache2-mod-php5 php5-mcrypt php5-curl php5-mysql php5-gd php5-cli php5-dev

apt-get -y install git python-pip python-dev libffi-dev libssl-dev libjpeg-dev libxml2-dev libxslt1-dev libmysqlclient-dev


echo -e "${RED}installing email tools ${NC}"
apt-get -y install sendemail libio-socket-ssl-perl libnet-ssleay-perl

echo -e "${RED}pip install...${NC}"


pip install mysqlclient mitmproxy

echo -e "${RED}Configuring apache2${NC}"

echo "Overwriting /etc/apache2/sites-available/000-default.conf ..."


echo "<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>" > /etc/apache2/sites-available/000-default.conf

echo -e "${RED}restarting  apache2  + mysql${NC}"

service apache2 restart && service mysql restart

echo -e "${RED}deleting content of /var/www/html/${NC}"

cd /var/www/html/
rm /var/www/html/*

echo -e "${RED}cloning from git repo${NC}"


git clone https://gitlab.com/l.haselauer/privacyproxy.git .


echo -e "${RED}creating config.php${NC}"

cp config.php.template config.php

# sed
sed -i -e "s/db_user_here/root/g" config.php
sed -i -e "s/db_password_here/$mysqlpw/g" config.php


echo -e "${RED}Chown /var/www/ to www-data ${NC}"

chown -R www-data:www-data /var/www/
echo "done"

echo -e "${RED}creating database entries ${NC}"

## Note: pp.sql is needed!
if [ -f setup/pp.sql ]
then
        mysql -u root -p$mysqlpw < setup/pp.sql
        #echo "mysql -u root -p$mysqlpw < setup/pp.sql"
else
        echo "File pp.sql not available... Cannot restore database. Go to privacyproxy.eu/setup to download the missing file"
fi


