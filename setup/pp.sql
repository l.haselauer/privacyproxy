-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 30, 2016 at 14:00 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pp`
--
CREATE DATABASE IF NOT EXISTS `pp` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `pp`;

-- --------------------------------------------------------

--
-- Table structure for table `blocklist`
--

CREATE TABLE IF NOT EXISTS `blocklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `blocklist`
--

INSERT INTO `blocklist` (`id`, `created`, `host`, `count`) VALUES
(1, '2016-01-01 12:00:00', 'example.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `host` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `source` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

-- --------------------------------------------------------

--
-- Table structure for table `searchlist`
--

CREATE TABLE IF NOT EXISTS `searchlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `host` varchar(255) NOT NULL,
  `searchpattern` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `searchlist`
--

INSERT INTO `searchlist` (`id`, `created`, `host`, `searchpattern`, `count`) VALUES
(1, '2016-01-01 12:00:00', '', 'lat(itude)?.{0,5}[0-9,\\.]{2,}', 0),
(2, '2016-01-01 12:00:00', '', 'long(itude)?.{0,5}[0-9,\\.]{2,}', 0),
(3, '2016-01-01 12:00:00', '', 'IMEI|imei|Imei', 0),
(4, '2016-01-01 12:00:00', '', 'MAC|mac|Mac', 0),
(5, '2016-01-01 12:00:00', '', '\\+[0-9/\\- ]{6,}', 0),
(6, '2016-01-01 12:00:00', '', '[a-z]{5,}@[a-z0-9\\-]+\\.[a-z0-9\\-]+', 0),
(7, '2016-01-01 12:00:00', '', 'UDID|udid|Udid', 0),
(8, '2016-01-01 12:00:00', '', '[Ll]oc(ation)?[^a-z]+', 0),
(9, '2016-01-01 12:00:00', '', '([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})', 0),
(10, '2016-01-01 12:00:00', '', '[Cc]arrier([-]?[Nn]ame)?|[Oo]perator|cn["=:]', 0),
(11, '2016-01-01 12:00:00', '', '([Pp]hone)[-]?([Nn]umber)?', 0);


-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE IF NOT EXISTS `temp` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp`
--

INSERT INTO `temp` (`key`, `value`) VALUES
('ip', ' ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
