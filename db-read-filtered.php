<?php
include "config.php";

$log = array();

// Create connection
$conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, created, host, source, data FROM requests where type = 1 order by id ASC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $log[] = array("id" => $row["id"], "data" => $row["data"], "source" => $row["source"], "host" => $row["host"], "created" => $row["created"]);
    }
    echo json_encode($log);
} else {
    echo "0 results";
}
$conn->close();
?>
