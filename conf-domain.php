<!DOCTYPE HTML>
<!--
	Dopetrope by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<?php
   include "config.php";

  // Create connection
  $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

//  $conn = new mysqli($servername, $username, $password, $dbname);
  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

   if (isset($_GET["unblock"])) {
     echo "<i>" . htmlspecialchars($_GET["unblock"]) . " removed from blacklist</i> ";
     $sql = "DELETE FROM blocklist where host='" . $_GET["unblock"] . "'";
     $conn->query($sql);

   }

   if (isset($_POST["block"])) {
     $sql = "INSERT INTO blocklist (host) values ('" . $_POST["block"] . "')";
     $conn->query($sql);

   }
?>

<html>
	<head>
		<title>Privacy Proxy</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div id="header">

						<!-- Logo -->
							<h1><a href="index.html">Privacy Proxy</a></h1>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li><a href="index.html">About</a></li>
									<li>
										<a href="setup-install-server.html">Setup</a>
										<ul>
											<li><a href="setup-install-server.html">Install Server</a></li>
											<li><a href="setup-conf-devices.html">Configure Devices</a></li>
											<li><a href="setup-rootca.html">Root CA Certificate</a></li>
										</ul>
									</li>
									<li class="current"><a href="info-up.php">Run</a>
										<ul>
											<li><a href="info-up.php">Up?</a></li>
											<li><a href="conf-pattern.php">Conf: Search</a></li>
											<li><a href="info-log-filter.html">Log: Search</a></li>
											<li><a href="conf-domain.php">Conf: Block</a></li>
											<li><a href="info-log-blocking.html">Log: Block</a></li>
										</ul>
									</li>
								</ul>
							</nav>

					</div>
				</div>

			<!-- Main -->
				<div id="main-wrapper">
					<div class="container">

						<!-- Content -->
							<article class="box post">
								<!--a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a -->
								<header>
									<h2>List of blocked domains</h2>
									<p>Any connection to one of these domains is blocked.</p>
								</header>
								<p>
<?php

   $sql = "SELECT host, count, created FROM blocklist order by count DESC";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    echo "<table>";
    echo "<tr style><th>Domain</th><th>Requests</th><th>Remove </th></tr>";

    // output data of each row
    while($row = $result->fetch_assoc()) {
	  echo "<tr><td> " . $row["host"]. "</td><td>" . $row["count"]. "</td>";
	  echo "<td><a href='?unblock=". $row["host"]. "'>X</td></tr>";

    }
   } else {
    echo "<tr>0 results</tr>";
   }
   echo "</table>";

?> 

								</p>
								<section>
									<header>
										<h3>Add Domains to Blocklist</h3>
									</header>
									<p>
    <form method=POST>
     <input type=text placeholder='example.com' name=block>
     <input type=submit value="Block Domain">
   </form>
									</p>
								</section>
								<section>
									<header>
										<h3>Possible further domains to block</h3>
									</header>
									<p>
										This is a list of domains that your previuously connected. Maybe you want do block some of these.
									</p>
									<p>
<?php
   $sql = "SELECT DISTINCT host from requests where host LIKE '%' AND host NOT in (SELECT host from blocklist)";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    echo "<ul>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
          echo "<li> " . $row["host"]. "</li>";

    }
    echo "</ul>";
   } else {
    echo "<tr>0 results</tr>";
   }
?>
								</p>
								</section>
							</article>

					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<section id="footer" class="container">

						<div class="row">
							<div class="12u">

								<!-- Copyright -->
									<div id="copyright">
										<ul class="links">
											<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
										</ul>
									</div>

							</div>
						</div>
					</section>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
