<!DOCTYPE HTML>
<!--
  Dopetrope by HTML5 UP
  html5up.net | @n33co
  Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
  -->
<html>
  <head>
    <title>Privacy Proxy</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/css/main.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="assets/css/ie8.css" />
    <![endif]-->
  </head>
  <body class="homepage">
    <div id="page-wrapper">
      <!-- Header -->
      <div id="header-wrapper">
        <div id="header">
          <!-- Logo -->
          <h1><a href="index.html">Privacy Proxy</a></h1>
          <!-- Nav -->
          <nav id="nav">
            <ul>
              <li><a href="index.html">About</a></li>
              <li>
                <a href="setup-install-server.html">Setup</a>
                <ul>
                  <li><a href="setup-install-server.html">Install Server</a></li>
                  <li><a href="setup-conf-devices.html">Configure Devices</a></li>
                  <li><a href="setup-rootca.html">Root CA Certificate</a></li>
                </ul>
              </li>
              <li class="current">
                <a href="info-up.php">Run</a>
                <ul>
                  <li><a href="info-up.php">Up?</a></li>
                  <li><a href="conf-pattern.php">Conf: Search</a></li>
                  <li><a href="info-log-filter.html">Log: Search</a></li>
                  <li><a href="conf-domain.php">Conf: Block</a></li>
                  <li><a href="info-log-blocking.html">Log: Block</a></li>
                </ul>
              </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- Main -->
      <div id="main-wrapper">
        <div class="container">
          <!-- Content -->
          <article class="box post">
            <!--a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a -->
            <header>
              <h2>Status</h2>
              <p>
                <?php
                  $o = exec("ps -aux | egrep \"mitm(dump|proxy|web)\"");
                   echo (strlen($o) >1 ? "Privacy Proxy is running." : "Privacy Proxy is <strong>not</strong> running");
                ?>
              </p>
            </header>
            <p>
            </p>
            <section>
              <header>
                <h3>Start / Stop</h3>
                <p>
                  Run or stop the Privacy Proxy. 
                </p>
              </header>
              <p>
              <form method=POST action=service.php>
                <input type=hidden name=start>
                <input type=submit value="Start Privacy Proxy" class="alt">
              </form>
              </br>
              <form method=POST action=service.php>
                <input type=hidden name=stop>
                <input type=submit value="Stop Privacy Proxy">
              </form>
              </p>
              <p>
              </p>
            </section>
            <section>
              <header>
                <h3>Stats</h3>
              </header>
              <p><strong>Uptime:</strong> <?=exec("ps -axo time,command | grep [m]itmdump | egrep -o \"^[^ ]*\""); ?></p>
              <p><strong>Server time:</strong> <?=date("Y-m-d H:i:s"); ?> </p>
              <p><strong>Logfile:</strong> <a href="/log.txt">Click here</a> </p>
              </p>
            </section>
          </article>
        </div>
      </div>
      <!-- Footer -->
      <div id="footer-wrapper">
        <section id="footer" class="container">
          <div class="row">
            <div class="12u">
              <!-- Copyright -->
              <div id="copyright">
                <ul class="links">
                  <li>&copy; Lukas Haselauer 2015-2016.</li>
                  <li>&copy; Design: <a href="http://html5up.net">HTML5 UP</a> <a href="https://creativecommons.org/licenses/by/3.0/">(CC Attribution 3.0 License)</a></li>
                  <li>&copy; Images: All Images on this project are Public Domain (CC0)</a></li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dropotron.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/skel-viewport.min.js"></script>
    <script src="assets/js/util.js"></script>
    <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
    <script src="assets/js/main.js"></script>
  </body>
</html>
