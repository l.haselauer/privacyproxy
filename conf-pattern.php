<!DOCTYPE HTML>
<!--
	Dopetrope by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->

   <?php
   include "config.php";

  // Create connection
  $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

  // Check connection
  if ($conn->connect_error) {
    echo "PW: '" . ini_get("mysqli.default_user") . "'";
    die("Connection failed: " . $conn->connect_error);
  }

   if (isset($_GET["remove"])) {
     $sql = "DELETE FROM searchlist where id='" . $_GET["remove"] . "'";
     $conn->query($sql);

   }

   if (isset($_POST["pattern"])) {
     $sql = "INSERT INTO searchlist (host, searchpattern) values ('" . $_POST["host"] ."','" . $_POST["pattern"] .  "')";
     $conn->query($sql);

   }
?>
<html>
	<head>
		<title>Privacy Proxy</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body class="homepage">
		<div id="page-wrapper">

			<!-- Header -->
				<div id="header-wrapper">
					<div id="header">

						<!-- Logo -->
							<h1><a href="index.html">Privacy Proxy</a></h1>

						<!-- Nav -->
							<nav id="nav">
								<ul>
									<li><a href="index.html">About</a></li>
									<li>
										<a href="setup-install-server.html">Setup</a>
										<ul>
											<li><a href="setup-install-server.html">Install Server</a></li>
											<li><a href="setup-conf-devices.html">Configure Devices</a></li>
											<li><a href="setup-rootca.html">Root CA Certificate</a></li>
										</ul>
									</li>
									<li class="current"><a href="info-up.php">Run</a>
										<ul>
											<li><a href="info-up.php">Up?</a></li>
											<li><a href="conf-pattern.php">Conf: Search</a></li>
											<li><a href="info-log-filter.html">Log: Search</a></li>
											<li><a href="conf-domain.php">Conf: Block</a></li>
											<li><a href="info-log-blocking.html">Log: Block</a></li>
										</ul>
									</li>
								</ul>
							</nav>

					</div>
				</div>

			<!-- Main -->
				<div id="main-wrapper">
					<div class="container">

						<!-- Content -->
							<article class="box post">
								<!--a href="#" class="image featured"><img src="images/pic01.jpg" alt="" /></a -->
								<header>
									<h2>List of patterns</h2>
									<p>Every request is searched for these patterns.</p>
								</header>
								<p>
<?php
   $sql = "SELECT id, host, searchpattern, count FROM searchlist order by count DESC";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    echo "<table style='border-collapse: collapse; width:90%;'>";
    echo "<tr style='background-color: #4CAF50;color: white;'><th>id</th><th>Domain</th><th>Pattern</th><th>Count</th><th>Edit </th></tr>";

    // output data of each row
    while($row = $result->fetch_assoc()) {
	  echo "<tr><td>" . $row["id"] . "</td><td> " . ($row["host"] ? htmlspecialchars($row["host"]) : "<i>any</i>" ). "</td><td>" . htmlspecialchars($row["searchpattern"]). "</td><td>" . $row["count"]. "</td>";
	  echo "<td><a href='?remove=". $row["id"]. "'>Remove</td></tr>";
//        echo "<li>" . $row["host"].
//        echo "<li>" . $row["host"]. "<form method=POST><input type='hidden' name='unblock' value='". $row["host"]."'><input type='submit' value='Submit'></form>  </li>";
    }
   } else {
    echo "<tr>0 results</tr>";
   }
   echo "</table>";
   ?>
								</p>
								<section>
									<header>
										<h3>Add entry</h3>
										<p>Enter a new search term</p>
									</header>
									<p>
    <form method=POST>
     <input type=text placeholder='example.com' name=host>
     <input type=text placeholder='Python Regex, eg. [a-z]*' name=pattern>
     <input type=submit value="Create Searchterm">
   </form>
									</p>
								</section>
								<section>
									<header>
										<h3>Suggested search terms</h3>
									</header>
									<p>
										<ul>
											<li>Your Phone Number</li>
											<li>Your Name</li>
											<li>Unique Identifiers of devices (Serial, MAC)</li>
										</ul>
									</p>
								</section>
							</article>

					</div>
				</div>

			<!-- Footer -->
				<div id="footer-wrapper">
					<section id="footer" class="container">

						<div class="row">
							<div class="12u">

								<!-- Copyright -->
									<div id="copyright">
										<ul class="links">
											<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
										</ul>
									</div>

							</div>
						</div>
					</section>
				</div>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/skel-viewport.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>

	</body>
</html>
