<?php

// My service file

include "config.php";



function mitm_start() {
	//echo "cgi-bin/runmitm.sh " . $usr . " " . $pwd . " " . $db . " > /dev/null 2>&1 &";
	exec("cgi-bin/runmitm.sh " . DB_USER . " " . DB_PASSWORD . " " . DB_NAME . "> /dev/null 2>&1 &");
}

function mitm_stop() {
	exec("killall runmitm.sh");
	exec("killall mitmdump");
	return 0;
}



if (isset($_POST["start"]))
	mitm_start();

if (isset($_POST["stop"]))
	mitm_stop();

if (isset($_POST["restart"])){
	mitm_stop();
	mitm_start();
}

header('Location: info-up.php');

?>
