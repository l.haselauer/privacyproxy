
from libmproxy.models import HTTPResponse
from netlib.http import Headers
import re
import MySQLdb
import urllib

cursor = None
db = None
srcip = "0.0.0.0"

#
# get the database credentials from the command arguments
#
def start(context, argv):
    if len(argv) != 4:
        raise ValueError('Usage: -s "privacy.py db-user db-password db-database"')
    print("DB Creds: " + str(argv[1:]))
    global db
    db = MySQLdb.connect("localhost", argv[1], argv[2], argv[3] )
    global cursor
    cursor  = db.cursor()

# testing sql conn
#    hostname = "example.com"
#    sql = "SELECT id, searchpattern FROM searchlist WHERE host LIKE %s OR host LIKE '';"
#    cursor.execute(sql, (hostname,))
#    results = cursor.fetchall()
#    if len(results) > 0:
#        for r in results:
#            print(str(r))


#
# check DB if hostname is blocked
#
def is_blocked(hostname):
    # split by dots
    splitname = hostname.split(".")
    for s in range(len(splitname)):
        namex= ".".join(splitname[len(splitname)-s-1:])
        sql = "SELECT id, host FROM blocklist WHERE host LIKE %s LIMIT 1;"
        print (namex)
        cursor.execute(sql, (namex,))
        results = cursor.fetchall()
        if len(results) > 0 and results[0][1].endswith(namex):
            print (namex + " matches!");
            sql = "UPDATE `blocklist` SET `count` = `count` +1 WHERE id = %s"
            try:
                cursor.execute(sql, (str(results[0][0]),))
                db.commit()
            except:
                # Rollback in case there is any error
                db.rollback()
            return 1
    return 0


#
# write new entry to DB
#
def db_entry(hostname, text, type):
    txt2 = re.sub(r'[^\x20-\x7f]',r'.',text) 

    sql = "INSERT INTO requests (host, data, type, source) VALUES (%s, %s, %s, (SELECT `value` from temp where `key` LIKE 'ip'));"
#    sql = sql.format(hostname, text, type) # old 'n dangerous
    try:
        cursor.execute(sql, (hostname, txt2, type,))
        db.commit()
    except:
        print ("Writing DB Entry failed!")
        # Rollback in case there is any error
        db.rollback()


#
# look for searchpatterns
#
def db_do_search(hostname, text):
    if len(text) < 2:
        return 0
        # We don't check that short texts

    text = urllib.unquote(text);
    sql = "SELECT id, searchpattern FROM searchlist WHERE host LIKE %s OR host LIKE '';"
    cursor.execute(sql, (hostname,))
    results = cursor.fetchall()
    fullrtxt = ""
    if len(results) > 0:
        for r in results:
#            print( text)
            try:
                m = re.search(r[1], text)
            except:
		print ("[ERROR] Regex is invalid: " + r[1])
            if m:
                # we have a match!
                sql = "UPDATE `searchlist` SET `count` = `count` +1 WHERE id = %s"
                try:
                    cursor.execute(sql, (str(r[0]),))
                    db.commit()
                except:
                    # Rollback in case there is any error
                    db.rollback()

                rtxt = m.group() #
                print (m.group() + " matches!")
                if m.start() < 50:
                    rtxt = text[:m.start()] + "<b>" + rtxt
                else:
                    rtxt = text[m.start()-50:m.start()] + "<b>" + rtxt
                if m.end()+ 50 > len(text):
                    rtxt = rtxt + "</b>" + text[m.end():]
                else:
                    rtxt = rtxt + "</b>" + text[m.end():m.end()+50]
                fullrtxt += "<div class='finding'>" + rtxt + "</div>" 
    if (len(fullrtxt)==0):
        return 0
    return fullrtxt




#
# Handle Requests
#
def request(context, flow):
    content = flow.request.content

    if flow.request.pretty_host.endswith("privacyproxy.eu"):
        return
    
    if is_blocked(flow.request.pretty_host):
        context.log("Request blacklisted")
        db_entry(flow.request.pretty_host, "BLOCKED", 2)
        context.kill_flow(flow)
        return
    else:
        pass
        #db_entry(flow.request.pretty_host, "PASSED")

    s = db_do_search(flow.request.pretty_host, flow.request.url + " " +flow.request.content)
    if s is not 0:
        db_entry(flow.request.pretty_host, str(s), 1)

#    s = db_do_search(flow.request.pretty_host, flow.request.url)
#    if s is not 0:
#        db_entry(flow.request.pretty_host, str(s), 1)

    

def clientconnect(context, root_layer):
    """
        Called when a client initiates a connection to the proxy. Note that a
        connection can correspond to multiple HTTP requests
    """
    context.log("clientconnect")

    # Hack: It seems that there is no option in mitmproxy to get the source ip except from the
    # root_layer object. Therefore, the ip of the connectiong host is stored in the DB.
    srcip = root_layer.client_conn.address.host    
    sql = "UPDATE `temp` SET `value` = %s WHERE `key` LIKE 'ip'"
    try:
        cursor.execute(sql, (scrip,))
        db.commit()
    except:
        # Rollback in case there is any error
        db.rollback()


