#!/bin/bash
# Starts the proxy (if it crashes with a non-graceful exit code it will auto-restart)

# Grab directory this script is located in

#cd /var/www/mitmproxy
#. ../venv.mitmproxy/bin/activate


function finish {
 echo "[`date`] Stopping mitmproxy" >> log.txt
}
trap finish EXIT



DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CMD="mitmdump -s '${DIR}/privacy.py $1 $2 $3'"
echo "[`date`] Starting mitmproxy..." >> log.txt

until eval $CMD; do
        echo "[`date`] Proxy crashed with exit code $?, restarting..." >> log.txt
        sleep 1
done

deactivate

exit 0
